package com.example.FX.Rate.Service.api;

import com.example.FX.Rate.Service.model.Exchange;
import com.example.FX.Rate.Service.service.ExchangeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExchangeControllerTest {
    @InjectMocks
    ExchangeController exchangeController;
    @Mock
    ExchangeService exchangeService;

    @Test
    public void testGetAllExchangeRates() {
        Date date = new Date(1587056440);
        Exchange testExchange = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        List<Exchange> exchangeMock = List.of(testExchange);
        when(exchangeService.getAllExchangeRates()).thenReturn(exchangeMock);
        List<Exchange> exchange = exchangeController.getAllExchangeRates();

        verify(exchangeService, times(1)).getAllExchangeRates();
        assertEquals(testExchange, exchange.get(0));

    }

    @Test
    public void getExchangeRateByBase() {
        Date date = new Date(1587056440);
        Exchange testExchange = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        Optional<Exchange> exchangeMock = Optional.of(testExchange);
        when(exchangeService.getExchangeRateByBase("GDP")).thenReturn(exchangeMock);
        Exchange exchange = exchangeController.getExchangeRateByBase("GDP");

        verify(exchangeService, times(1)).getExchangeRateByBase("GDP");
        assertEquals(testExchange, exchange);
    }
}