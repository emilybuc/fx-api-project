package com.example.FX.Rate.Service.model;

import org.junit.jupiter.api.Test;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;

class ExchangeTest {
    @Test
    void exchangeIsSetUpCorrectly() {
        Date date = new Date(1587056440);
        Exchange exchangeMockData = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        assertEquals(exchangeMockData.getBase(),"GDP");
        assertEquals(exchangeMockData.getDate(),date);
        assertEquals(exchangeMockData.getGDP(),1);
        assertEquals(exchangeMockData.getEUR(),2);
        assertEquals(exchangeMockData.getUSD(),3);
        assertEquals(exchangeMockData.getAUD(),4);
        assertEquals(exchangeMockData.getJPY(),5);

    }


}