package com.example.FX.Rate.Service.dao;

import com.example.FX.Rate.Service.model.Exchange;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ExchangeDataAccessServiceTest {
    @InjectMocks
    ExchangeDataAccessService exchangeDataAccessService;
    @Mock
    JdbcTemplate jdbcTemplate;
    @Test
    void selectAllExchangeRates() {
        ReflectionTestUtils.setField(exchangeDataAccessService, "jdbcTemplate", jdbcTemplate);
        Date date = new Date(1587056440);
        Exchange exchangeMockData = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        List<Exchange> listOfExchangeMockData = List.of(exchangeMockData);
        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(listOfExchangeMockData);
        List<Exchange> exchange = exchangeDataAccessService.selectAllExchangeRates();

        assertEquals(exchangeMockData, exchange.get(0));
    }

    @Test
    void selectExchangeRateByBase() {
        ReflectionTestUtils.setField(exchangeDataAccessService, "jdbcTemplate", jdbcTemplate);
        Date date = new Date(1587056440);
        Exchange exchangeMockData = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        Optional<Exchange> OptionalOfExchangeMockData = Optional.of(exchangeMockData);
        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class))).thenReturn(OptionalOfExchangeMockData);
        //You cannot put in "actual strings" and then stubbed items in one test, mockito doesnt like it, either use all real world data or all stubbed items
        //        when(jdbcTemplate.queryForObject("SELECT * FROM exchange", aRealClass.class(some args)).thenReturn(OptionalOfExchangeMockData); **this wouldn't work**
        //        when(jdbcTemplate.queryForObject(anyString(), any(RowMapper.class))).thenReturn(OptionalOfExchangeMockData); -> all stubbed data
        Optional<Exchange> exchange = exchangeDataAccessService.selectExchangeRateByBase("GDP");

        assertEquals(exchangeMockData, exchange.get());
    }
}