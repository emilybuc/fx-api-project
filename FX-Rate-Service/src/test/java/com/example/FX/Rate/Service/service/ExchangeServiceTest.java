package com.example.FX.Rate.Service.service;

import com.example.FX.Rate.Service.dao.ExchangeDao;
import com.example.FX.Rate.Service.model.Exchange;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ExchangeServiceTest {
    @InjectMocks
    ExchangeService exchangeService;
    @Mock
    ExchangeDao exchangeDao;

    @Test
    public void getAllExchangeRates() {
        Date date = new Date(1587056440);
        Exchange exchangeMockData = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        List<Exchange> listOfExchangeMockData = List.of(exchangeMockData);
        when(exchangeDao.selectAllExchangeRates()).thenReturn(listOfExchangeMockData);
        List<Exchange> exchange = exchangeService.getAllExchangeRates();

        verify(exchangeDao, times(1)).selectAllExchangeRates();
        assertEquals(exchangeMockData, exchange.get(0));
    }

    @Test
    public void getExchangeRateByBase() {
        Date date = new Date(1587056440);
        Exchange exchangeMockData = new Exchange("GDP", date, 1, 2, 3, 4, 5);
        Optional<Exchange> optionalOfExchangeMockData = Optional.of(exchangeMockData);
        when(exchangeDao.selectExchangeRateByBase("GDP")).thenReturn(optionalOfExchangeMockData);
        Optional<Exchange> exchange = exchangeService.getExchangeRateByBase("GDP");

        verify(exchangeDao, times(1)).selectExchangeRateByBase("GDP");
        assertEquals(exchangeMockData, exchange.get());
    }
}