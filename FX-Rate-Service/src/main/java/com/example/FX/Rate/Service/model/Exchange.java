package com.example.FX.Rate.Service.model;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.sql.Date;

public class Exchange {
    private String base;
    private Date date;
    private float GDP;
    private float EUR;
    private float USD;
    private float AUD;
    private float JPY;

    public Exchange(@JsonProperty("base") String base,
                    @JsonProperty("date") Date date,
                    @JsonProperty("GDP") float GDP,
                    @JsonProperty("EUR") float EUR,
                    @JsonProperty("USD") float USD,
                    @JsonProperty("AUD") float AUD,
                    @JsonProperty("JPY") float JPY) {
        this.base = base;
        this.date = date;
        this.GDP = GDP;
        this.EUR = EUR;
        this.USD = USD;
        this.AUD = AUD;
        this.JPY = JPY;
    }


    public Date getDate() {
        return date;
    }

    public float getAUD() {
        return AUD;
    }

    public float getEUR() {
        return EUR;
    }

    public float getGDP() {
        return GDP;
    }

    public float getJPY() {
        return JPY;
    }

    public float getUSD() {
        return USD;
    }

    public String getBase() {
        return base;
    }
}
