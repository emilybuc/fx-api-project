CREATE TABLE exchange (
    base VARCHAR(4) NOT NULL PRIMARY KEY,
    date DATE NOT NULL,
    gdp FLOAT,
    eur FLOAT,
    usd FLOAT,
    aud FLOAT,
    jpy FLOAT
);