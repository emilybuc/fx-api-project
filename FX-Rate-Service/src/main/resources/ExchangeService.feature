Feature: The service should allow to put delete and update on Exchange Rate service
  Scenario: The service should return all exchange rates
    Given Create inputs for the service to query
    When I use a get on my API it fetches the whole database
    Then The result is the whole database